from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import patterns, include, url
from django.contrib import admin


admin.autodiscover()


urlpatterns = patterns('',
                       url(r'^home/', 'adaptiware.views.home', name='home'),
                       url(r'^contact/', 'adaptiware.views.contact', name='contact'),
                       url(r'^portfolio/', 'adaptiware.views.portfolio', name='portfolio'),
                       url(r'^admin/', include(admin.site.urls)),
                       )+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
