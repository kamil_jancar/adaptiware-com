from django.db import models

# Create your models here.
class Portfolio(models.Model):
  name = models.CharField(max_length=30)
  client = models.CharField(max_length=20)
  image = models.ImageField(upload_to='gallery',blank=True, null=True, help_text="Your Portfolio image")
  launched = models.DateField()
  url = models.URLField()
  
  
  def __unicode__(self):
      return self.name
    