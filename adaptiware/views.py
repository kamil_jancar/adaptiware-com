from django.shortcuts import render_to_response
from django.template import RequestContext
from adaptiware.models import Portfolio


  
def home(request):
    return render_to_response(
        'adaptiware/home.html',
        context_instance=RequestContext(request)
    )
  
def contact(request):
    return render_to_response(
        'adaptiware/contact.html',
        context_instance=RequestContext(request)
    )
  
def portfolio(request):
    images = Portfolio.objects.all()
    img_var = {'images' : images}
    return render_to_response(
        'adaptiware/portfolio.html',
        context_instance=RequestContext(request,img_var)
    )
